package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"ovpnadmin/internal/app/ovpnadmin/argconfig"
	"ovpnadmin/internal/app/ovpnadmin/configure"
	"ovpnadmin/internal/app/ovpnadmin/endpoints"
	"ovpnadmin/internal/app/ovpnadmin/middleware"
	"ovpnadmin/internal/app/ovpnadmin/pages"
	"ovpnadmin/internal/pkg/database"
)

func main() {
	var err error
	var logFile *os.File

	argConfig := argconfig.New()
	argConfig.GetArgs()

	conf := configure.New()
	err = conf.Read(argConfig.ConfigFile)
	fatalError(err)

	db := database.New()
	err = db.Open(conf.Database)
	fatalError(err)

	en := endpoints.New(conf, db)
	fatalError(err)

	pg := pages.New(conf, db)

	logFile, err = os.OpenFile(conf.OvpnApiLogFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	fatalError(err)
	defer logFile.Close()

	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile | log.Lmsgprefix)
	log.SetOutput(logFile)

	http.HandleFunc("/", pg.GetAllOnlineVpnConnections)
	http.HandleFunc("/abonents", pg.GetAllAbonents)
	http.HandleFunc("/certs", pg.GetAllCertificates)
	http.HandleFunc("/api/vpn/list", middleware.CheckToken(en.GetAllOnlineVpnConnections, conf.Token))
	http.HandleFunc("/api/abonent/list", middleware.CheckToken(en.GetAllAbonents, conf.Token))
	http.HandleFunc("/api/cert/list", middleware.CheckToken(en.GetAllCertificates, conf.Token))

	log.Printf("start api server on %s:%d", conf.ListenIp, conf.Port)
	http.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir(conf.StaticFiles))))
	err = http.ListenAndServe(fmt.Sprintf("%s:%d", conf.ListenIp, conf.Port), nil)
	fatalError(err)
}

func fatalError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
