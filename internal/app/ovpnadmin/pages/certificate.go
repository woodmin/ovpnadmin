package pages

import (
	"fmt"
	"log"
	"net/http"
	"ovpnadmin/internal/pkg/api/certificate"
	"text/template"
)

func (pg *Pages) GetAllCertificates(w http.ResponseWriter, r *http.Request) {
	var err error
	var certificates certificate.Certificates

	certificates, err = certificate.GetAllCerts(pg.Conf, pg.Database)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	tmpl, err := template.ParseFiles(fmt.Sprintf("%s/certs.html", pg.Conf.TemplatesFiles),
		fmt.Sprintf("%s/default.html", pg.Conf.TemplatesFiles))
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	err = tmpl.Execute(w, certificates)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

}
