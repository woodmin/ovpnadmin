package pages

import (
	"fmt"
	"log"
	"net/http"
	"ovpnadmin/internal/pkg/api/abonent"
	"text/template"
)

func (pg *Pages) GetAllAbonents(w http.ResponseWriter, r *http.Request) {
	var err error
	var abonents abonent.Abonents

	abonents, err = abonent.GetAllAbonents(pg.Database)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	tmpl, err := template.ParseFiles(fmt.Sprintf("%s/abonents.html", pg.Conf.TemplatesFiles),
		fmt.Sprintf("%s/default.html", pg.Conf.TemplatesFiles))
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	err = tmpl.Execute(w, abonents)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

}
