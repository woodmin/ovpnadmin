package pages

import (
	"ovpnadmin/internal/app/ovpnadmin/configure"
	"ovpnadmin/internal/pkg/database"
)

type Pages struct {
	Conf     configure.Conf
	Database database.DataBase
}

func New(conf configure.Conf, db database.DataBase) Pages {
	return Pages{Conf: conf, Database: db}
}
