package pages

import (
	"fmt"
	"log"
	"net/http"
	"ovpnadmin/internal/pkg/api/vpn"
	"text/template"
)

func (pg *Pages) GetAllOnlineVpnConnections(w http.ResponseWriter, r *http.Request) {
	var err error
	var vpnConnections vpn.VpnConnections

	vpnConnections, err = vpn.GetAllOnlineVpnConnections(pg.Conf, pg.Database)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	tmpl, err := template.ParseFiles(fmt.Sprintf("%s/online.html", pg.Conf.TemplatesFiles),
		fmt.Sprintf("%s/default.html", pg.Conf.TemplatesFiles))
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	err = tmpl.Execute(w, vpnConnections)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

}
