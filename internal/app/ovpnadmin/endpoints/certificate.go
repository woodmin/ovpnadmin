package endpoints

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"ovpnadmin/internal/pkg/api/certificate"
)

func (en *Endpoints) GetAllCertificates(w http.ResponseWriter, r *http.Request) {
	var err error
	var data []byte
	var certificates certificate.Certificates
	certificates, err = certificate.GetAllCerts(en.Conf, en.Database)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	data, err = json.Marshal(certificates)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	w.Write(data)
}
