package endpoints

import (
	"ovpnadmin/internal/app/ovpnadmin/configure"
	"ovpnadmin/internal/pkg/database"
)

type Endpoints struct {
	Conf     configure.Conf
	Database database.DataBase
}

func New(conf configure.Conf, dataBase database.DataBase) Endpoints {
	return Endpoints{Conf: conf, Database: dataBase}
}
