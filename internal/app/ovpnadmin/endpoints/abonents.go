package endpoints

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"ovpnadmin/internal/pkg/api/abonent"
)

func (en *Endpoints) GetAllAbonents(w http.ResponseWriter, r *http.Request) {
	var err error
	var data []byte
	var abonents abonent.Abonents

	abonents, err = abonent.GetAllAbonents(en.Database)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	data, err = json.Marshal(abonents)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	w.Write(data)
}
