package endpoints

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"ovpnadmin/internal/pkg/api/vpn"
)

func (en *Endpoints) GetAllOnlineVpnConnections(w http.ResponseWriter, r *http.Request) {
	var err error
	var data []byte
	var vpnConnections vpn.VpnConnections

	vpnConnections, err = vpn.GetAllOnlineVpnConnections(en.Conf, en.Database)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	data, err = json.Marshal(vpnConnections)
	if err != nil {
		log.Println(err)
		http.Error(w, fmt.Sprintf("%v", err), 500)
		return
	}

	w.Write(data)
}
