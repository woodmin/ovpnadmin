package configure

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Conf struct {
	ListenIp                   string `yaml:"ListenIp"`
	Port                       int    `yaml:"Port"`
	Database                   string `yaml:"Database"`
	TemplatesFiles             string `yaml:"TemplatesFiles"`
	StaticFiles                string `yaml:"StaticFiles"`
	CaCertFile                 string `yaml:"CaCertFile"`
	CrtCertFiles               string `yaml:"CrtCertFiles"`
	KeyCertFiles               string `yaml:"KeyCertFiles"`
	OvpnLogFile                string `yaml:"OvpnLogFile"`
	OvpnLogStatusFile          string `yaml:"OvpnLogStatusFile"`
	OvpnApiLogFile             string `yaml:"OvpnApiLogFile"`
	StartLineFirstStage        string `yaml:"StartLineFirstStage"`
	EndLineFirstStage          string `yaml:"EndLineFirstStage"`
	StartLineSecondStage       string `yaml:"StartLineSecondStage"`
	EndLineSecondStage         string `yaml:"EndLineSecondStage"`
	FieldCertFirstStage        int    `yaml:"FieldCertFirstStage"`
	FieldExtIpFirstStage       int    `yaml:"FieldExtIpFirstStage"`
	FieldVpnIpFirstStage       int    `yaml:"FieldVpnIpFirstStage"`
	FieldUploadFirstStage      int    `yaml:"FieldUploadFirstStage"`
	FieldDownloadFirstStage    int    `yaml:"FieldDownloadFirstStage"`
	FieldDataConnectFirstStage int    `yaml:"FieldDataConnectFirstStage"`
	FieldCertTwoStage          int    `yaml:"FieldCertTwoStage"`
	FieldVpnIpTwoStage         int    `yaml:"FieldVpnIpTwoStage"`
	Token                      string `yaml:"Token"`
}

func New() Conf {
	return Conf{}
}

func (conf *Conf) Read(confiFile string) error {
	var err error
	var file []byte
	file, err = os.ReadFile(confiFile)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(file, &conf)
	if err != nil {
		return err
	}

	return nil
}
