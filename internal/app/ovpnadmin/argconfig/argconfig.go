package argconfig

import (
	"flag"
	"os"
)

type ArgConfig struct {
	ConfigFile string
	Command    string
}

func New() ArgConfig {
	return ArgConfig{}
}

func (arg *ArgConfig) GetArgs() {
	argConfig := flag.String("config", "", "--config <path/filename.yaml>")
	argCommand := flag.String("cmd", "", "--cmd <command> or <>")
	flag.Parse()
	if *argConfig == "" {
		flag.Usage()
		os.Exit(0)
	}

	arg.ConfigFile = *argConfig
	arg.Command = *argCommand

}
