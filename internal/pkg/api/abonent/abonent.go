package abonent

import (
	"database/sql"
	"ovpnadmin/internal/pkg/database"

	_ "github.com/mattn/go-sqlite3"
)

type Abonent struct {
	Cert        string `yaml:"Cert"`
	Name        string `yaml:"Name"`
	Address     string `yaml:"Address"`
	Contact     string `yaml:"Contact"`
	Description string `yaml:"Description"`
}

type Abonents []Abonent

func GetAllAbonents(db database.DataBase) (Abonents, error) {
	var err error
	var rows *sql.Rows
	var abonent Abonent
	var abonents Abonents

	rows, err = db.Conn.Query("SELECT * FROM abonents")
	if err != nil {
		return abonents, err
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&abonent.Cert, &abonent.Name, &abonent.Address, &abonent.Contact, &abonent.Description)
		if err != nil {
			return abonents, err
		}

		abonents = append(abonents, abonent)
	}

	return abonents, err
}

func GetOneAbonentByCert(db database.DataBase, cert string) (Abonent, error) {
	var err error
	var abonent Abonent

	return abonent, err
}
