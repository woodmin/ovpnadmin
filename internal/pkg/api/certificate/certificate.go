package certificate

import (
	"bufio"
	"io/fs"
	"os"
	"ovpnadmin/internal/app/ovpnadmin/configure"
	"ovpnadmin/internal/pkg/api/abonent"
	"ovpnadmin/internal/pkg/database"
	"path/filepath"
	"strings"
	"time"
)

type Certificate struct {
	Name        string
	DateExpired time.Time
	Abonent     abonent.Abonent
}

type Certificates []Certificate

func GetAllCerts(conf configure.Conf, db database.DataBase) (Certificates, error) {
	var err error
	var certsDir []fs.DirEntry
	var file *os.File
	var certificates Certificates
	var certificate Certificate
	var abonents abonent.Abonents

	abonents, err = abonent.GetAllAbonents(db)
	if err != nil {
		return certificates, err
	}

	certsDir, err = os.ReadDir(conf.CrtCertFiles)
	if err != nil {
		return certificates, err
	}

	for _, certFile := range certsDir {
		file, err = os.Open(filepath.Join(conf.CrtCertFiles, certFile.Name()))
		if err != nil {
			return certificates, err
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {

			lineExpired := strings.TrimSpace(scanner.Text())
			if strings.HasPrefix(lineExpired, "Not After :") {
				lineExpired = strings.TrimSpace(strings.Split(lineExpired, " : ")[1])
				lineExpired = strings.ReplaceAll(lineExpired, "  ", " ")
				certificate.Name = strings.TrimSuffix(certFile.Name(), filepath.Ext(certFile.Name()))
				certificate.DateExpired, err = time.Parse("Jan 2 15:04:05 2006 GMT", lineExpired)
				if err != nil {
					return nil, err
				}

				for _, abonent := range abonents {
					if certificate.Name == abonent.Cert {
						certificate.Abonent = abonent
						break
					}
				}
				certificates = append(certificates, certificate)

			}

		}

	}

	return certificates, err
}
