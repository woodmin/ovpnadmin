package vpn

import (
	"bufio"
	"os"
	"ovpnadmin/internal/app/ovpnadmin/configure"
	"ovpnadmin/internal/pkg/api/abonent"
	"ovpnadmin/internal/pkg/database"

	"strings"
)

type VpnConnection struct {
	Cert        string
	ExtIp       string
	VpnIp       string
	Network     string
	Hosts       []string
	Abonent     abonent.Abonent
	Upload      string
	Download    string
	TimeConnect string
}

type VpnConnections []VpnConnection

func GetAllOnlineVpnConnections(conf configure.Conf, db database.DataBase) (VpnConnections, error) {
	var err error
	var file *os.File
	var lines []string
	var abonents []abonent.Abonent
	var vpnConnections VpnConnections

	file, err = os.Open(conf.OvpnLogStatusFile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	vpnConnections, err = parseFileFirstStatge(lines, vpnConnections, conf)
	if err != nil {
		return nil, err
	}

	vpnConnections, err = parseFileSecondStatge(lines, vpnConnections, conf)
	if err != nil {
		return nil, err
	}

	abonents, err = abonent.GetAllAbonents(db)
	if err != nil {
		return nil, err
	}

	vpnConnections, err = addAbonents(abonents, vpnConnections)
	if err != nil {
		return nil, err
	}

	return vpnConnections, nil

}

func addAbonents(abonents abonent.Abonents, vpnConnections VpnConnections) (VpnConnections, error) {
	var err error

	for i, vpnConnection := range vpnConnections {
		for _, ab := range abonents {
			if vpnConnection.Cert == ab.Cert {
				vpnConnections[i].Abonent = ab

			}
		}
	}

	return vpnConnections, err
}

func parseFileFirstStatge(lines []string, vpnConnections VpnConnections, conf configure.Conf) (VpnConnections, error) {
	var err error
	var filter bool
	var vpnConnection VpnConnection
	for _, line := range lines {
		if strings.HasPrefix(line, conf.EndLineFirstStage) {
			break
		}

		if filter {
			fields := strings.Split(line, ",")
			vpnConnection.Cert = fields[conf.FieldCertFirstStage]
			vpnConnection.ExtIp = strings.Split(fields[conf.FieldExtIpFirstStage], ":")[0]
			vpnConnection.Upload = fields[conf.FieldUploadFirstStage]
			vpnConnection.Download = fields[conf.FieldDownloadFirstStage]
			vpnConnection.TimeConnect = fields[conf.FieldDataConnectFirstStage]
			vpnConnections = append(vpnConnections, vpnConnection)
		}

		if strings.HasPrefix(line, conf.StartLineFirstStage) {
			filter = true
		}

	}

	return vpnConnections, err
}

func parseFileSecondStatge(lines []string, vpnConnections VpnConnections, conf configure.Conf) (VpnConnections, error) {
	var err error
	var filter bool
	var linesSecondStage []string

	for _, line := range lines {
		if strings.HasPrefix(line, conf.EndLineSecondStage) {
			break
		}
		if filter {
			linesSecondStage = append(linesSecondStage, line)
		}
		if strings.HasPrefix(line, conf.StartLineSecondStage) {
			filter = true
		}
	}

	for i, vpnConnection := range vpnConnections {
		for _, line := range linesSecondStage {
			fields := strings.Split(line, ",")
			if fields[conf.FieldCertTwoStage] == vpnConnection.Cert {
				if strings.Contains(fields[conf.FieldVpnIpTwoStage], "/") {
					vpnConnections[i].Network = fields[conf.FieldVpnIpTwoStage]
				} else if strings.Contains(fields[conf.FieldVpnIpTwoStage], "C") {
					vpnConnections[i].Hosts = append(vpnConnections[i].Hosts, strings.ReplaceAll(fields[conf.FieldVpnIpTwoStage], "C", ""))
				} else {
					vpnConnections[i].VpnIp = fields[conf.FieldVpnIpTwoStage]
				}
			}
		}
	}

	return vpnConnections, err

}
