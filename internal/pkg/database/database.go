package database

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

type DataBase struct {
	Conn *sql.DB
}

func New() DataBase {
	return DataBase{}
}

func (d *DataBase) Open(dbFile string) error {
	var err error
	d.Conn, err = sql.Open("sqlite3", dbFile)
	return err
}
