CREATE TABLE abonents (
    Cert        TEXT PRIMARY KEY NOT NULL,
    Name        TEXT NOT NULL,
    Address     TEXT NOT NULL,
    Contact     TEXT NOT NULL,
    Description TEXT NOT NULL
);
