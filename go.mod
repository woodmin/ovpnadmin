module ovpnadmin

go 1.22.0

require gopkg.in/yaml.v2 v2.4.0

require github.com/mattn/go-sqlite3 v1.14.22
